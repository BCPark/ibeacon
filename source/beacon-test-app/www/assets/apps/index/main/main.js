;(function (window, _, $, angular, undefined) {
    var module = angular.module("app");

    module.directive("main", function () {
        var scope = {};

        var templateUrl = "assets/apps/index/main/main.html";

        var controller = ["$scope", "$element", "reqToSvr", "$beacon", function ($scope, $element, reqToSvr, $beacon) {
            var default_uuid1 = '401a55cf-666d-4aa7-b3e4-11e465ab1b12';
            var default_uuid2 = 'a52e9268-06fe-42c0-843c-069f4bb591db';
            var default_major = 0;

            var options = [
                {identifier: "beacon_1", uuid: default_uuid1, major: default_major, minor: 1},
                {identifier: "beacon_2", uuid: default_uuid1, major: default_major, minor: 2},
                {identifier: "beacon_3", uuid: default_uuid2, major: default_major, minor: 1},
                {identifier: "beacon_4", uuid: default_uuid2, major: default_major, minor: 2}
            ];

            $scope.msgs = {};
            $scope.msgs2 = {};

            $scope.init = function () {
                $beacon.setOptions(options,
                    function (res) {
                        console.log("res", res);

                        var msg = {};

                        msg.identifier = res.region.identifier;

                        if ($scope.msgs2[msg.identifier]) {
                            $scope.msgs2[msg.identifier].uuid = res.region.uuid;
                            $scope.msgs2[msg.identifier].minor = res.region.minor;
                            $scope.msgs2[msg.identifier].major = res.region.major;
                            $scope.msgs2[msg.identifier].state = res.state;
                        }
                        else {
                            msg.uuid = res.region.uuid;
                            msg.minor = res.region.minor;
                            msg.major = res.region.major;
                            msg.state = res.state;

                            $scope.msgs2[msg.identifier] = msg;
                        }

                        var state = res.state == "CLRegionStateInside" ? "inside" : "outside";

                        $scope.$digest();
                        navigator.notification.alert(msg.identifier + " : " + state, function(){}, state, "Confirm");
                        // alert(msg.identifier + " : " + state);
                        console.log(msg.identifier, state);

                    }, function (res) {
                        console.log("res", res);

                        var msg = {};

                        msg.identifier = res.region.identifier;

                        if ($scope.msgs[msg.identifier]) {
                            if (res.beacons.length > 0) {
                                $scope.msgs[msg.identifier].uuid = res.beacons[0].uuid;
                                $scope.msgs[msg.identifier].minor = res.beacons[0].minor;
                                $scope.msgs[msg.identifier].major = res.beacons[0].major;
                                $scope.msgs[msg.identifier].acc = res.beacons[0].accuracy;
                                $scope.msgs[msg.identifier].proximity = res.beacons[0].proximity;
                            }
                        }
                        else {
                            if (res.beacons.length > 0) {
                                msg.uuid = res.beacons[0].uuid;
                                msg.minor = res.beacons[0].minor;
                                msg.major = res.beacons[0].major;
                                msg.acc = res.beacons[0].accuracy;
                                msg.proximity = res.beacons[0].proximity;

                                $scope.msgs[msg.identifier] = msg;
                            }
                        }
                        $scope.$digest();
                        // if(res.beacons.length > 0 && res.beacons[0].proximity == "ProximityNear")
                        //     navigator.notification.alert(msg.identifier + " : near", function(){}, "ProximityNear", "Confirm");
                    });
            };

            document.addEventListener("deviceready", function () {
                $scope.init();
            });


            $scope.rangeStart = function () {
                $beacon.rangeStart(function (e) {
                    console.log("range start success", e);
                }, function (e) {
                    console.log("range stop error", e);
                });
                $scope.isRange = true;
            };

            $scope.rangeStop = function () {
                $beacon.rangeStop(function (e) {
                    console.log("range stop success", e);
                }, function (e) {
                    console.log("range stop error", e);
                });
                $scope.isRange = false;
            };

            $scope.monitoringStart = function () {
                $beacon.monitoringStart(function (e) {
                    console.log("monitoring start success", e);
                }, function (e) {
                    console.log("range error", e);
                });

                $scope.isRange = false;
            };

            $scope.monitoringStop = function () {
                $beacon.monitoringStop(function (e) {
                    console.log("monitoring stop success", e);
                }, function (e) {
                    console.log("range error", e);
                });
                $scope.isRange = true;
            };
        }];

        return {
            restrict: "E",
            scope: scope,
            templateUrl: templateUrl,
            replace: true,
            controller: controller
        };
    });

})(window, _, jQuery, angular);
