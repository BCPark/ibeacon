;(function (window, _, $, angular, FastClick, undefined) {

	var module = angular.module("app", ["ui.router"]);

	module.config(["$httpProvider", "$compileProvider", function ($httpProvider, $compileProvider) {
		$httpProvider.defaults.headers.common["ajax-request"] = "true";
	}]);

	module.provider("reqToSvr", function () {
		var map = {};

		this.register = function (name, func) {
			map[name] = func;
		};

		this.$get = ["$injector", function ($injector) {
			return function (name, paramObj) {
				var func = map[name];
				if (!func) throw Error("Not Registered");

				return $injector.invoke(func, undefined, paramObj);
			};
		}];
	});

	$(function () {
		FastClick.attach(window.document.body);
	});
})(window, _, jQuery, angular, FastClick);
