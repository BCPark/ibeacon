;(function (window, _, $, angular, undefined) {
	var module = angular.module("app");

	module.config(["$stateProvider", "$urlRouterProvider",
		function ($stateProvider, $urlRouterProvider) {
			$stateProvider.state("main", {
				url: "/main",
				template: "<main></main>"
			});

			//--------------------------------------------------------------------------------

			$urlRouterProvider.otherwise("/main");
		}]);
})(window, _, jQuery, angular);
