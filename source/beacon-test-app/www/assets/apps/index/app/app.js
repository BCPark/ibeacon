;(function (window, _, $, angular, undefined) {
	var module = angular.module("app");

	module.directive("app", function () {
		var scope = {};

		var templateUrl = "assets/apps/index/app/app.html";

		var controller = ["$scope", "$element", function ($scope, $element) {

		}];

		return {
			restrict: "E",
			scope: scope,
			templateUrl: templateUrl,
			replace: true,
			controller: controller
		};
	});

	module.directive("koInput", function () {
		return {
			priority: 2,
			restrict: "A",
			link: {
				pre: function (scope, element) {
					$(element).bind("compositionstart", function (event) {
						event.stopImmediatePropagation();
					});
				}
			}
		};
	});

})(window, _, jQuery, angular);
