;(function (window, _, $, angular, undefined) {
    var module = angular.module("app");

    module.factory("$beacon", ["$q", function ($q) {
        var beaconOptions = [];
        var delegate = null;

        var setOptions = function (options, monitoringFn, rangeFn) {
            beaconOptions = options;

            delegate = new cordova.plugins.locationManager.Delegate();

            delegate.didRangeBeaconsInRegion = rangeFn;
            delegate.didDetermineStateForRegion = monitoringFn;

            cordova.plugins.locationManager.setDelegate(delegate);
            cordova.plugins.locationManager.requestAlwaysAuthorization();
        };

        var rangeStart = function (succesFn, errorFn) {
            _.forEach(beaconOptions, function (option) {
                var beacon = new cordova.plugins.locationManager.BeaconRegion(option.identifier, option.uuid, option.major, option.minor);
                // cordova.plugins.locationManager.requestWhenInUseAuthorization();
                cordova.plugins.locationManager.startRangingBeaconsInRegion(beacon).fail(errorFn).done(succesFn);
                console.log(option.uuid, "range start");
            });
        };

        var rangeStop = function (succesFn, errorFn) {
            _.forEach(beaconOptions, function (option) {
                var beacon = new cordova.plugins.locationManager.BeaconRegion(option.identifier, option.uuid, option.major, option.minor);
                cordova.plugins.locationManager.stopRangingBeaconsInRegion(beacon).fail(errorFn).done(succesFn);
                console.log(option.uuid, "range stop");
            });
        };

        var monitoringStart = function (succesFn, errorFn) {
            _.forEach(beaconOptions, function (option) {
                var beacon = new cordova.plugins.locationManager.BeaconRegion(option.identifier, option.uuid, option.major, option.minor);
                // cordova.plugins.locationManager.requestWhenInUseAuthorization();
                cordova.plugins.locationManager.startMonitoringForRegion(beacon).fail(errorFn).done(succesFn);
                console.log(option.uuid, "monitoring start");
            });
        };

        var monitoringStop = function (succesFn, errorFn) {
            _.forEach(beaconOptions, function (option) {
                var beacon = new cordova.plugins.locationManager.BeaconRegion(option.identifier, option.uuid, option.major, option.minor);
                cordova.plugins.locationManager.stopMonitoringForRegion(beacon).fail(errorFn).done(succesFn);
                console.log(option.uuid, "monitoring stop");
            });
        };

        return {
            setOptions: setOptions,
            rangeStart: rangeStart,
            rangeStop: rangeStop,
            monitoringStart: monitoringStart,
            monitoringStop: monitoringStop
        };
    }]);

})(window, _, jQuery, angular);
